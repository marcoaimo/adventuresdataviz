'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({

    browserSync: {
      dev:{
        bsFiles: {
          src : [
              '*', 
              'css/*.css', 
              'js/*', 'views/**/*',
              'app/index.html'
              ]
        },
        options: {
          server: {
              baseDir: "./app/",
          },
          open: false
        }
      }
    },

  });

  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.registerTask('dev', ['browserSync:dev']);
  grunt.registerTask('default', [ 'dev' ]);

};
