(function (angular) {
	'use strict'

	angular
		.module('DatavizApp')
		.config(RouteConfig)

	/* @ngInject */
	function RouteConfig ($stateProvider, $urlRouterProvider) {

		$urlRouterProvider.otherwise('/')

		$stateProvider
			.state('landing', {
				url: '/',
				templateUrl: 'views/landing.html',
				controller: 'LandingCtrl',
				controllerAs: 'landingCtrl'
			})
			.state('home', {
				url: '/survey',
				templateUrl: 'views/home.html',
				controller: 'MainCtrl',
				controllerAs: 'mainCtrl'
			})
			.state('bubble', {
				url: '/bubble',
				params: {
          userdata: null
        },
				templateUrl: 'views/bubble.html',
				controller: 'BubbleCtrl',
				controllerAs: 'bubbleCtrl'
			})
			.state('tab', {
				url: '/tab',
				templateUrl: 'views/tab.html',
				controller: 'TabCtrl',
				controllerAs: 'tabCtrl'
			})
			.state('tab2', {
				url: '/tab2',
				templateUrl: 'views/tab2.html',
				controller: 'Tab2Ctrl',
				controllerAs: 'tab2Ctrl'
			})
			.state('radar', {
				url: '/radar',
				templateUrl: 'views/radar.html',
				controller: 'RadarCtrl',
				controllerAs: 'radarCtrl'
			})
			.state('map', {
				url: '/map',
				templateUrl: 'views/map.html',
				controller: 'MapCtrl',
				controllerAs: 'mapCtrl'
			})
	}
}(window.angular))
