(function (angular) {
	'use strict'

	angular
		.module('DatavizApp', [
			'ui.router'
		])
}(window.angular))
