(function (angular) {
  'use strict'

  angular
    .module('DatavizApp')
    .controller('Bubble2Ctrl', bubble2Ctrl)

  /* @ngInject */
  function bubble2Ctrl ($scope) {
    var vm = this

var fear = ['150','150px','50px','30px','175px'],
	fearAvg = ['50px','30px','100px','30px', '150px'],
	bt0,
	bt00,
	blob0,
	blob00,
	bt1,
	bt11,
	blob1,
	blob11
	;

var bottons = [],
	divi = []
	;

for(var i = 0; i< fear.length; i++){

	bottons.push(document.createElement('button')) ;
	bottons[i].setAttribute('id','component-'+i);
	divi.push(document.createElement('div').setAttribute('id','contenitore'+i));
	console.log(divi);
	divi[i].appendChild(bottons[i]);

}


function getRandom(min, max){
	return Math.random() * (max - min) + min;
}

Array.prototype.slice.call(document.querySelectorAll('.button'), 0).forEach(function(bt) {
	bt.addEventListener('click', function(e) {
		e.preventDefault();
	});
});



( function loop (){
	setTimeout(function () {
		initBt(blob0,blob00);
		initBt(blob1,blob11);
		loop();
	}, 60);
})();




function initBt(blob, blob1) {
	var
		dirX = Math.random() > 0.84 ? -1 : 1,
		dirY = Math.random() > 0.8 ? -1 : 1,
		r = getRandom(60, 80)
		;


	Array.prototype.slice.call(blob, 1).forEach(function(bt) {
		var tl = new TimelineLite();
		tl.to(bt, 1.2, { x: dirX * r * Math.random() + '%', y: dirY * r * Math.random() + '%', ease: Elastic.easeOut.config(1, 0.2) });
		tl.to(bt, 1.2, { x: '0%', y: '0%', ease: Elastic.easeOut.config(1, 0.2) }, '-=1.1');
	});
	Array.prototype.slice.call(blob1, 1).forEach(function(bt) {
		var tl1 = new TimelineLite();
		tl1.to(bt, 1.2, { x: dirX * r * Math.random() + '%', y: dirY * r * Math.random() + '%', ease: Elastic.easeOut.config(1, 0.2) });
		tl1.to(bt, 1.2, { x: '0%', y: '0%', ease: Elastic.easeOut.config(1, 0.2) }, '-=1.1');
	});

}

  }
}(window.angular))
