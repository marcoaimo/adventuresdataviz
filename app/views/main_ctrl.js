(function (angular) {
  'use strict'

  angular
    .module('DatavizApp')
    .controller('MainCtrl', mainCtrl)

  /* @ngInject */
  function mainCtrl ($scope, $state) {
    var vm = this

    $scope.title = 'BUBBLES'
    $scope.xxx = xxx

    $scope.mydata = {
    	assault: 0,
			car: 0,
			fraud: 0,
			theft: 0,
			privacy: 0,
			kanton: null
    }

    function xxx () {
    	$scope.mydata.assault = document.getElementsByName('assault')[0].value
    	$scope.mydata.car = document.getElementsByName('car')[0].value
    	$scope.mydata.fraud = document.getElementsByName('fraud')[0].value
    	$scope.mydata.theft = document.getElementsByName('theft')[0].value
    	$scope.mydata.privacy = document.getElementsByName('privacy')[0].value
    	$scope.mydata.kanton = document.getElementsByName('kanton')[0].value
    	console.log($scope.mydata)
    	$state.go('bubble', {userdata: $scope.mydata})
    }
  }
}(window.angular))
