(function (angular) {
  'use strict'

  angular
    .module('DatavizApp')
    .controller('TabCtrl', tabCtrl)

  /* @ngInject */
  function tabCtrl ($scope) {
    var vm = this

    $scope.setType = setType
    $scope.drop = dropGraph
    $scope.setDataset = setDataset
    $scope.dataset = '_Dataset_Criminality_subset1_assault.csv'
    $scope.title = $scope.dataset
    $scope.graphtype = 'type1'

 		var svg = d3.select("svg"),
        margin = {top: 20, right: 20, bottom: 30, left: 40},
        width  = +svg.attr("width") - margin.left - margin.right,
        height = +svg.attr("height") - margin.top - margin.bottom,
        g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var x = d3.scaleBand()
			        .rangeRound([0, width])
			        .padding(0.1)
			        .align(0.1);
    var y = d3.scaleLinear()
        			.rangeRound([height, 0]);
    var z = d3.scaleOrdinal()
        			.range(["#f0c7c0","#e68470"]);
    var stack = d3.stack();

    function setDataset (dataset) {
    	$scope.title = dataset
    	$scope.dataset = dataset
    	dropGraph()
    	$scope.graphtype = 'type1'
    	reloadGraph($scope.graphtype, 1000)
    }

    function setType(type) {
      reloadGraph(type, 200)
    }

    loadGraph($scope.graphtype)
    function loadGraph (graphtype) {
    	var callback = null

      if(graphtype === 'type0') callback = type0
      if(graphtype === 'type1') callback = type1
      if(graphtype === 'type2') callback = type2
      d3.csv("assets/csv/criminality_swiss_years/" + $scope.dataset,callback,
        function(error, data){
          if (error) throw error;

          buildGraph(data)
        })
    }

    function reloadGraph (graphtype, reloadDelay) {
    	var callback = null

      if(graphtype === 'type0') callback = type0
      if(graphtype === 'type1') callback = type1
      if(graphtype === 'type2') callback = type2
      d3.csv("assets/csv/criminality_swiss_years/" + $scope.dataset,callback,
        function(error, data){
          if (error) throw error;

          updateGraph(data, reloadDelay)
        })
    }

    function dropGraph () {
    	g.selectAll(".serie")
		    .selectAll("rect")
		    .transition()
	      	.delay(function(d,i) { return 200 + (30*i)})
	      	.duration(500)
		      .attr("y", function(d) { return height })
		      .attr("height", function(d) { return 0 })
    }
    
    function updateGraph (data, reloadDelay) {
      data.sort(function(a, b) { return b.total - a.total; });

	    x.domain(data.map(function(d) { return d.Year; }));
	    y.domain([0, d3.max(data, function(d) { 
	    	var tot = d.total
	    	if (tot > 2000) return tot+=2000
	    	return tot += 400
	    })]).nice();
	    z.domain(data.columns.slice(1));

		  g.selectAll(".serie")
		    .data(stack.keys(data.columns.slice(1))(data))
		    .transition()
		    .attr("fill", function(d) { return z(d.key); })
		  
		  g.selectAll(".serie")
		    .data(stack.keys(data.columns.slice(1))(data))
		    .selectAll("rect")
		    .data(function(d) { return d; })
		    .transition()
	      	.delay(function(d,i) { return reloadDelay + (30*i)})
	      	.duration(500)
		      .attr("y", function(d) { return y(d[1]); })
		      .attr("height", function(d) { return y(d[0]) - y(d[1]); })

		  var yaxis = d3.selectAll(".axis--y")
		      .transition()
		      .duration(300)
		      .delay(300)
		      .call(d3.axisLeft(y).ticks(10, "s"))

		  var legend = g.selectAll(".legend")
		    .data(data.columns.slice(1).reverse())
				.selectAll("text")
		  		.transition()
		      .text(function(d) { return d })
		};

    function buildGraph (data) {
      data.sort(function(a, b) { return b.total - a.total; });

	    x.domain(data.map(function(d) { return d.Year; }));
	    y.domain([0, d3.max(data, function(d) {
	    	var tot = d.total
	    	if (tot > 2000) return tot+=2000
	    	return tot += 400
	    })]).nice();
	    z.domain(data.columns.slice(1));

		  g.selectAll(".serie")
		    .data(stack.keys(data.columns.slice(1))(data))
		    .enter().append("g")
		      .attr("class", "serie")
		      .attr("fill", function(d) { return z(d.key); })
		    .selectAll("rect")
		    .data(function(d) { return d; })
		    .enter()
		      .append("rect")
		      .attr("x", function(d) { return x(d.data.Year); })
		      .attr("y", function(d) { return y(height); })
		      .attr("height", 0)
		      .attr("width", x.bandwidth())
		      .transition()
		      	.delay(function(d,i) { return 200 + (30*i)})
		      	.duration(500)
			      .attr("y", function(d) { return y(d[1]); })
			      .attr("height", function(d) { return y(d[0]) - y(d[1]); })

		  g.append("g")
		      .attr("class", "axis axis--x")
		      .attr("transform", "translate(0," + height + ")")
		      .call(d3.axisBottom(x));

		  g.append("g")
		      .attr("class", "axis axis--y")
		      .call(d3.axisLeft(y).ticks(10, "s"))
		    .append("text")
		      .attr("x", 2)
		      .attr("y", y(y.ticks(10).pop()))
		      .attr("dy", "0.35em")
		      .attr("text-anchor", "start")
		      .attr("fill", "#000")
		      .text("Number of crime per year");

		  // var legend = g.selectAll(".legend")
		  //   .data(data.columns.slice(1).reverse())
		  //   .enter().append("g")
		  //     .attr("class", "legend")
		  //     .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; })
		  //     .style("font", "10px sans-serif");

		  // legend.append("rect")
		  //     .attr("x", width - 18)
		  //     .attr("width", 18)
		  //     .attr("height", 18)
		  //     .attr("fill", z);

		  // legend.append("text")
		  //     .attr("x", width - 24)
		  //     .attr("y", 9)
		  //     .attr("dy", ".35em")
		  //     .attr("text-anchor", "end")
		  //     .text(function(d) { return d; });
		};

		function calcTotal(d,i,columns) {
			for (var i = 1, t = 0; i < columns.length; ++i) t += d[columns[i]] = +d[columns[i]];
		  d.total = t;
		  return d;
		}

		function type0(d, i, columns) {
	    var x = columns.indexOf("Sum of Total Completed")
	    if(x >= 0) {
	    columns.splice(x,1)
	    }
	    var y = columns.indexOf("Sum of Total Aptempted")
	    if(y >= 0) {    
	    columns.splice(y,1)
	    }
	    var x = columns.indexOf("Sum of Total Solved")
	    if(x >= 0) {
	    columns.splice(x,1)
	    }
	    var y = columns.indexOf("Sum of Total Unsolved")
	    if(y >= 0) {    
	    columns.splice(y,1)
	    }
		    
		  return calcTotal(d,i,columns)
		}

		function type1(d, i, columns) {
	    var x = columns.indexOf("Sum of Total Completed")
	    if(x >= 0) {
	    columns.splice(x,1)
	    }
	    var y = columns.indexOf("Sum of Total Aptempted")
	    if(y >= 0) {    
	    columns.splice(y,1)
	    }
	    var z = columns.indexOf("Total Crime")
	    if(z >= 0) {    
	    columns.splice(z,1)
	    }
		    
		  return calcTotal(d,i,columns)
		}
		function type2(d, i, columns) {
	    var x = columns.indexOf("Sum of Total Solved")
	    if(x >= 0) {
	    columns.splice(x,1)
	    }
	    var y = columns.indexOf("Sum of Total Unsolved")
	    if(y >= 0) {    
	    columns.splice(y,1)
	    }
	    var z = columns.indexOf("Total Crime")
	    if(z >= 0) {    
	    columns.splice(z,1)
	    }
		    
		  return calcTotal(d,i,columns)
		}
  
  }
}(window.angular))
