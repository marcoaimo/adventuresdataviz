(function (angular) {
	'use strict'

	angular
		.module('DatavizApp')
		.controller('MapCtrl', mapCtrl)

	/* @ngInject */
	function mapCtrl ($scope) {
		var vm = this

		$scope.title = 'Map'

		d3.xml("assets/images/switzerlandmap2.svg").mimeType("image/svg+xml").get(function(error, xml) {
            if (error) throw error;
            document.getElementById('map').appendChild(xml.documentElement);
            loadData()
        });
        
        
        
        
        
        function loadData(){
            
            d3.csv('assets/csv/_Dataset_Criminality_subset1_cantons2015.csv', function(data){
                  
                  var byCanton = d3.nest()
                    .key(function(d){
                        return d.Canton
                    })
                    .entries(data)
                  
                 // byCanton.forEach(function(d, i){
                 //     console.log(d.key)
                 // })
        

                 d3.select('svg')
                    .select('#canton')
                    .selectAll('path')
                    .data(byCanton)
                    .on('mouseenter', function(d, i){
                     
                        d3.select(this).style('fill', '#82d8c2')
                        var id = d3.select(this).attr('id')
                        console.log(d, id, i)
                        
                        d3.select('svg')
                            .select('#rect')
                            .selectAll('rect')
                            .data(d.values)
                            .transition()
                            .attr('fill', '#7f7f7f')
                            .attr('width', function(c, j){
                                return c.percent*10
                                
                            var width = d3.scale.linear()
                            .domain([0, 100]) //d3.max(dataset) // <- HERE SHOULD BE d3.max(dataset) CORRECT?
                            .range([0,h]);
 
                                
                                
                            })
                        

                    })
                 
                 
                 
                 
                    .on('mouseleave', function(){
                     
                        d3.select(this).style('fill', '#f0624e')
                    })
        
        
            }) 
            
        }

	}
}(window.angular))
