(function (angular) {
  'use strict'

  angular
    .module('DatavizApp')
    .controller('BubbleCtrl', bubbleCtrl)

  /* @ngInject */
  function bubbleCtrl ($scope, $stateParams) {
    var vm = this

    $scope.title = 'BUBBLES'
    console.log($stateParams)

    var fake = { 
  		assault: 0,
			car: 0,
			fraud: 0,
			theft: 0,
			privacy: 0
		}

		var me = $stateParams.userdata || fake

		var dispatch = d3.dispatch('transitionToMenu');

    var diameter = 960, height = 600,
    format = d3.format(",d");
    var scaleFactor = 1.5
    // _.max(_.values(fake))

		var svg = d3.select("#bubbles")
		    .attr("width", diameter)
		    .attr("height", height)
		  	.append("g")

		d3.csv("assets/csv/fearfake.csv", function(error, data) {
		  if (error) throw error;

		  data = _.map(data, function (d) {
		  	var elem = {
		  		answer: d.answer*scaleFactor,
		  		category: d.category
		  	}
		  	return elem
		  })
		  me = _.mapValues(me, function (v, k) {
		  	return v
		  })

			var positions = [ {x:diameter/2, y:height/5} ]
		  data = _.map(data, function (d,i) {
		  	var elem = {
		  		category: '',
		  		me: 0,
		  		medium: 0,
		  		cx: positions[i].x,
		  		cy: positions[i].y
		  	}

		  	elem.category = d.category
	  		elem.me 			= me[d.category]
	  		elem.medium 	= d.answer

	  		console.log(elem, i)
  			var new_position = {}
  			// car
  			if (i === 0) {
  				new_position = {
		  			x: elem.cx + _.max([elem.me,parseFloat(elem.medium),me[data[i+1].category],parseFloat(data[i+1].answer)])*1.5,
		  			y: elem.cy + _.max([elem.me,parseFloat(elem.medium),me[data[i+1].category],parseFloat(data[i+1].answer)])*1.5
		  		}
  			}
  			// fraud
  			if (i === 1) {
		  		new_position = {
		  			x: elem.cx - _.max([elem.me,parseFloat(elem.medium),me[data[0].category],parseFloat(data[0].answer)]) *2,
		  			y: elem.cy + _.max([elem.me,parseFloat(elem.medium),me[data[0].category],parseFloat(data[0].answer)])
		  		}
  			}
  			// theft
  			if (i === 2) {
  				new_position = {
		  			x: elem.cx - _.max([elem.me,parseFloat(elem.medium),me[data[i+1].category],parseFloat(data[i+1].answer)]) *1.6,
		  			y: elem.cy - _.max([elem.me,parseFloat(elem.medium),me[data[i+1].category],parseFloat(data[i+1].answer)]) *1.5
		  		}
  			}
  			// privacy
  			if (i === 3) {
  				new_position = {
		  			x: elem.cx + _.max([elem.me,parseFloat(elem.medium),me[data[i-1].category],parseFloat(data[i-1].answer)]) *3.2,
		  			y: elem.cy + _.max([elem.me,parseFloat(elem.medium),me[data[i-1].category],parseFloat(data[i-1].answer)]) *3.2
		  		}
  			}
		  	positions.push(new_position)

		  	return elem
		  })

		  var colors 		= ["#f9ebaa", "#d3e4b5", "#b8dccd", "#b5cde5", "#f0c7c0"]
		  var medcolors = ["#e4ce6b", "#9dbd59", "#5eb992", "#5783b3", "#e68470"]
		  var z = d3.scaleOrdinal()
        			.range(medcolors);
      var zz = d3.scaleOrdinal()
        			.range(colors);



		  var circles = svg.selectAll('circle')
									  	 .data(data).enter()
									  	 .append('g')
									  	 .attr('id', 'circles')
									  	 .on('click', function () {
									  	 		transitionToMenu()
									  	 })

		  circles.append('circle')
		  			 .attr('class', 'bubble')
				  	 .attr('r', 0)
				  	 .attr('cx', function (d,i) {
				  	 		return d.cx
				  	 })
				  	 .attr('cy', function (d,i) {
				  	 		return d.cy
				  	 })
				  	 .style('fill', function(d,i){
				  	 	return z(i)
				  	 })
				  	 .transition()
				  	 	 .duration(500)
      			 	 .delay(function(d,i) {return 300 + 300*i})
      			 	 .attr('r', function (d,i) {
					  	 		return d.medium
					  	 })

		  circles.append('circle')
		  			 .attr('class', 'bubble')
				  	 .attr('r', 0)
				  	 .attr('cx', function (d,i) {
				  	 		return d.cx
				  	 })
				  	 .attr('cy', function (d,i) {
				  	 		return d.cy
				  	 })
				  	 .style('fill', function(d,i){
				  	 	return zz(i)
				  	 })
				  	 .transition()
				  	 	 .duration(500)
      			 	 .delay(function(d,i) {return 1000 + 300*i})
      			 	 .attr('r', function (d,i) {
					  	 		return d.me
					  	 })

			circles.append('text')
						 .attr("dx", function (d,i) {
				  	 		return d.cx
				  	 })
						 .attr("dy", function (d,i) {
				  	 		return d.cy
				  	 })
      			 .style("text-anchor", "middle")
      			 .attr('opacity', 0)
      			 .transition()
      			 	 .duration(300)
      			 	 .delay(function(d,i) {return 1600 + 300*i})
      			 	 .attr('opacity', 1)
	      			 .text(function (d,i) {
					  	 		return d.category
					  	 })

		})

		function transitionToMenu () {
			console.log('exit')
			// d3.selectAll('circle')
			// 	.transition()
			// 	.duration(600)
			// 	.delay(function(d,i){
			// 		return 300*i
			// 	})
			// 	.attr('cx', function(d,i) {
			// 		return i*20
			// 	})
			// 	.attr('cy', 20)
			// 	.attr('r', 20)

			// d3.selectAll('text')
			// 	.transition()
			//  	 .duration(200)
			//  	 .attr('opacity', 0)
  	// 		 .text(function (d,i) {
		 //  	 		return d.category
		 //  	 })

		}

  }
}(window.angular))
