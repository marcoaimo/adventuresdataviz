(function (angular) {
  'use strict'

  angular
    .module('DatavizApp')
    .controller('LandingCtrl', landingCtrl)

  /* @ngInject */
  function landingCtrl ($scope) {
    var vm = this

    $scope.title = 'Landing'

    $('#next').on('mouseenter', function(){
	    TweenLite.to($('#crocini'), 0.666, {delay: 0.3, opacity: 0.7})
	  })
	  $('#next').on('mouseleave', function(){
	    TweenLite.to($('#crocini'), 0.666, {delay: 0.3, opacity: 0.0})
	  })
  }
  
}(window.angular))
